// SOAL 1
var nilai = 78;

if (nilai >= 85) {
    console.log("A");
} else if (nilai < 85 && nilai >= 75) {
    console.log("B");
} else if (nilai < 75 && nilai >= 65) {
    console.log("C");
} else if (nilai < 65 && nilai >= 55) {
    console.log("D");
} else if (nilai < 55) {
    console.log("E");
}



// SOAL 2
var tanggal = 11;
var bulan = 12;
var tahun = 2000;

tanggal = tanggal.toString();
tahun = tahun.toString();

switch (bulan) {
    case 1: {
        console.log(tanggal.concat(" Januari ").concat(tahun));
        break;
    }
    case 2: {
        console.log(tanggal.concat(" Februari ").concat(tahun));
        break;
    }
    case 3: {
        console.log(tanggal.concat(" Maret ").concat(tahun));
        break;
    }
    case 4: {
        console.log(tanggal.concat(" April ").concat(tahun));
        break;
    }
    case 5: {
        console.log(tanggal.concat(" Mei ").concat(tahun));
        break;
    }
    case 6: {
        console.log(tanggal.concat(" Juni ").concat(tahun));
        break;
    }
    case 7: {
        console.log(tanggal.concat(" Juli ").concat(tahun));
        break;
    }
    case 8: {
        console.log(tanggal.concat(" Agustus ").concat(tahun));
        break;
    }
    case 9: {
        console.log(tanggal.concat(" September ").concat(tahun));
        break;
    }
    case 10: {
        console.log(tanggal.concat(" Oktober ").concat(tahun));
        break;
    }
    case 11: {
        console.log(tanggal.concat(" November ").concat(tahun));
        break;
    }
    case 12: {
        console.log(tanggal.concat(" Desember ").concat(tahun));
        break;
    }
    default: {
        "Input Salah!";
    }
}



// SOAL 3
var n = 3;
var tigaPagar = "";
for (var i = 1; i <= n; i++) {
    for (var j = 0; j < i; j++) {
        tigaPagar += "#";
    }
    tigaPagar += "\n";
}
console.log(tigaPagar);

n = 7;
var tujuhPagar = "";
for (var i = 1; i <= n; i++) {
    for (var j = 0; j < i; j++) {
        tujuhPagar += "#";
    }
    tujuhPagar += "\n";
}
console.log(tujuhPagar);



// SOAL 4
var m = 10;
var j = 1;
var k = 2;

for (i = 3; i <= m; i++) {

    if (j == 1 || i % 3 == 1) {
        console.log(j + ' - I love programming');
        j += 3;
    }

    if (k == 2 || i % 3 == 2) {
        console.log(k + ' - I love Javascript');
        k += 3;
    }

    if (i % 3 == 0) {
        console.log(i + ' - I love VueJS');
    }

    if (i % 3 == 0) {;
        var string = ""
        for (let b = 1; b <= i; b++) {
            string += "="
        }
        console.log(string);
    }
}