
// SOAL 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var ketiga = pertama.substr(0,4);
var keempat = pertama.substr(12,6);
var kelima = kedua.substr(0,7);
var keenam = kedua.substr(8,10).toUpperCase();

console.log(ketiga.concat(" ").concat(keempat).concat(" ").concat(kelima).concat(" ").concat(keenam)); 




// SOAL 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var intPertama = parseInt(kataPertama);
var intKedua = parseInt(kataKedua);
var intKetiga = parseInt(kataKetiga);
var intKeempat = parseInt(kataKeempat);

console.log((intPertama + intKeempat) + (intKedua * intKetiga));




// SOAL 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga= kalimat.substring(15, 18);
var kataKeempat= kalimat.substring(19, 24);
var kataKelima= kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);














