// // SOAL 1 :
let luas = (p,l) => { 
    return p*l };
console.log('luasnya =',luas(10,8));


let keliling = (p, l) => { 
    return 2 * (p + l) };
console.log('kelilingnya =',keliling(10,5));




// SOAL 2 :
newFunction = (firstName, lastName) => {
    firstName
    lastName
    return {
        fullName() {
            return console.log(firstName + " " + lastName)
        }
    }
}
newFunction("William", "Imoh").fullName();


// // OPSI LAIN SOAL 2 :
const newFunction = (firstName, lastName) => {
  return `${firstName} ${lastName}`;
}
console.log(newFunction("William", "Imoh"));




// SOAL 3 :
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby)




// // SOAL 4 :
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined)




// SOAL 5 :
const planet = "earth" 
const view = "glass" 
console.log(`lorem ${planet}, dolor sit amet, consectetur adipiscing elit, ${view}`);

// OPSI LAIN SOAL 5 :
const planet = "earth" 
const view = "glass" 
var before = `lorem ${planet}, dolor sit amet, consectetur adipiscing elit, ${view}`
console.log(before);
